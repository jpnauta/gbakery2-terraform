locals {
  app_name = var.app_name
  environment = lookup(var.workspace_to_environment_map, terraform.workspace)
  project = var.project
  region = var.region
  zone = var.zone
  service_account_name = var.service_account_name
}

# Store TF state in GCS
terraform {
  backend "gcs" {
    bucket = "gbakery2-terraform"
    prefix = "terraform/state"
    credentials = "./config/account.json"
  }
}

# Configure gcloud
provider "google" {
  credentials = file("./config/account.json")
  project = local.project
  region = local.region
}

# Configure beta features for gcloud
# https://www.terraform.io/docs/providers/google/provider_versions.html
provider google-beta {
  credentials = file("./config/account.json")
  project = local.project
  region = local.region
}

# Reference to the access token currently used to authenticate the user (i.e. `config/account.json`)
data "google_client_config" "current" {}

# k8s cluster module
module "cluster" {
  source = "./modules/cluster"
  app_name = local.app_name
  environment = local.environment
  project = local.project
  region = local.region
  zone = local.zone
  service_account_name = local.service_account_name
  current_access_token = data.google_client_config.current.access_token
}
