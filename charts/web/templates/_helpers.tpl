{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "web.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "web.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "web.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Script to define regular environment variables for app
*/}}
{{- define "web.envscript" -}}

echo $SERVICE_ACCOUNT > ./credentials.json

{{- end -}}

{{/*
Script to wait for system to be ready before starting main task
*/}}
{{- define "web.initscript" -}}
until wget -qO- ${STATIC_URL}channels/js/websocketbridge.js > /dev/null;
    do echo "waiting for collectstatic to complete...";
    sleep 2;
done;

until [[  -z  $(./manage.py showmigrations | grep '\[ \]') ]];
    do echo "waiting for migrations to complete...";
    sleep 2;
done;
{{- end -}}