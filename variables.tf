variable "app_name" {
  description = "Name of the application."
  type = string
  default = "gbakery2"
}

variable "project" {
  description = "The Google Cloud project ID."
  type = string
  default = "gbakery2-153922"
}

variable "region" {
  description = "The Google Cloud region."
  type = string
  default = "us-west1"
}

variable "zone" {
  description = "The Google Cloud zone."
  type = string
  default = "us-west1-b"
}

variable "service_account_name" {
  description = "The name of the service account used to authenticate the Google provider."
  type = string
  default = "gbakery2-terraform"
}

variable "workspace_to_environment_map" {
  type = map

  default = {
    production = "production"
  }
}
