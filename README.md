# gbakery2 deployments

Handles provisioning infrastructure for Glamorgan Bakery

## Pre-requisites

- [Docker for Desktop](https://www.docker.com/products/docker-desktop)

If installing locally (not recommended), you will need:

- [Terraform](https://www.terraform.io/) (`brew install terraform`)
- [Helm 3](https://www.terraform.io/) *(not v2)*

## Getting Started

### Authentication

To authenticate with Google Cloud, you will need to obtain a service account key. To do so, go to the
[project IAM page](https://console.cloud.google.com/iam-admin/serviceaccounts?project=gbakery2-153922) and
download a JSON service key for the service account `gbakery2-terraform`. Place the service account
at the path `config/account.json` (relative to this project's folder). Each workspace
`production/sandbox` will need service accounts and their key files added to the project.

### Running the shell

Everything in this project can be done using the `shell` command.

```
make shell
```

This command boots up a bash shell inside a docker container containing the `terraform`, `gcloud` 
and `kubectl` commands installed.

### Quick Deploy

If you wish to deploy changes without booting up a shell, use the `deploy` command

```
make deploy
```

This will boot up a container and apply the necessary changes without any prompts.

## Terraform Usage

To apply changes via Terraform, you will need to install the required modules using the `init`
command. This should only need to be done on the first installation and running of the specific workspace

```
terraform init
```

Now you can run the main command to apply changes.

```
terraform apply
```

## Google Cloud CLI Usage

Before using the `gcloud` command, you will need to configure your service account
using the `gcloud_config` command

```
gcloud_config
```

## Kubernetes/Helm CLI Usage

Before using the `kubectl` or `helm` commands, you will need to authenticate with your cluster
using the `kube_config` command.

```
kube_config
```

## Best practices

* Use `kebab-case` to separate words. Do not use `PascalCase`, `camelCase`, `snake_case` or any other casing approach.
* For environment specific login, use `environment_to_[variable]_map` combined with `${lookup(var.environment_to_[var]_map, local.environment)}`
* Don't hardcode sensitive passwords here! Instead, `random_id` or Google Secrets Manager instead.

## Under the Hood

### State configuration

The state for each Terraform workspace is stored in this 
[GCS bucket](https://console.cloud.google.com/storage/browser/gbakery2-terraform-tf/terraform/state/?project=gbakery2-153922).
This is where sensitive passwords are stored and where lock files are created to ensure multiple deployments do not occur
at the same time.
 
### Common Errors

* `403: not authorized`:  You likely need to add IAM permissions to `gbakery2-terraform`
[here](https://console.cloud.google.com/iam-admin/iam?project=gbakery2-153922).
* `Error locking state`: This can occur if terraform is running simultaneously on two machines, or if terraform
    quit abnormally. You can unlock the lock using the command `terraform force-unlock [lock-id]`.  
* `The connection to the server localhost:8080 was refused`: run `kube_config` to authenticate with the cluster
