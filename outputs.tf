output "region" {
  description = "The Google Cloud region."
  value = var.region
}

output "zone" {
  description = "The Google Cloud zone."
  value = var.zone
}

output "project" {
  description = "The Google Cloud project ID."
  value = var.project
}

output "cluster_name" {
  description = "The name of the k8s cluster."
  value = module.cluster.cluster_name
}

output "postgresql_user_password" {
  value = module.cluster.postgresql_user_password
}

output "redis_user_password" {
  value = module.cluster.redis_user_password
}

output "web_static_ip" {
  value = module.cluster.web_static_ip
}