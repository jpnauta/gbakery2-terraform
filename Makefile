define run_command
	docker-compose down
	docker-compose build
    docker-compose run --rm deployments $(1)
endef
shell:
	$(call run_command,bash)
test_structure:
	docker-compose -f docker-compose.yml build
	docker-compose -f docker-compose.test.structure.yml build
	docker-compose -f docker-compose.test.structure.yml up --abort-on-container-exit
clean:
	docker-compose -f docker-compose.yml down
	docker-compose -f docker-compose.test.structure.yml down
deploy:
	$(call run_command,terraform apply)
