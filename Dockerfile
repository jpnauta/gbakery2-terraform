FROM python:3.9-alpine

# Enable SSL
RUN apk -q --update add ca-certificates wget curl tar

# Install gcloud and kubectl
ENV PATH /google-cloud-sdk/bin:$PATH
ENV CLOUDSDK_PYTHON_SITEPACKAGES 1
RUN cd / && \
    wget https://dl.google.com/dl/cloudsdk/channels/rapid/google-cloud-sdk.zip && \
    unzip google-cloud-sdk.zip && \
    rm google-cloud-sdk.zip && \
    google-cloud-sdk/install.sh --usage-reporting=true --path-update=true --bash-completion=true --rc-path=/.bashrc --additional-components app kubectl alpha beta

# Install terraform
ENV TERRAFORM_VERSION 0.14.11
RUN cd /tmp/ && \
    curl -sS -o terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform.zip && \
    rm terraform.zip && \
    mv terraform /usr/local/bin/ && \
    # Utility to parse terraform json output
    apk -q --update add jq

# Install Helm
ENV HELM_VERSION v3.7.1
RUN cd /tmp/ && \
    curl -sS -o helm.tar.gz https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -zxvf helm.tar.gz -C /tmp && \
    mv linux-amd64/helm /bin/helm && \
    rm -rf linux-amd64/helm && \
    # Helm plugins require git
    # helm-diff requires bash, curl
    apk -q --no-cache --update add git bash

WORKDIR /app/

# Add scripts
ADD commands/ ./commands/
RUN chmod +x ./commands/*
ENV PATH="/app/commands:${PATH}"

# Add files
ADD modules/ ./modules/
ADD config/ ./config/
ADD charts/ ./charts/
ADD main.tf .
ADD variables.tf .
ADD outputs.tf .
