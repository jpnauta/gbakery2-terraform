data "google_client_config" "default" {
  provider = google-beta
}

# Random password for master auth user
resource "random_id" "cluster_user_password" {
  byte_length = 8
}

# Primary k8s cluster
resource "google_container_cluster" "primary" {
  provider = google-beta  # Enable beta features
  name = "${local.app_name}-${local.environment}-cluster"
  location = local.zone

  remove_default_node_pool = true
  initial_node_count = 1

  enable_legacy_abac = true

  cluster_autoscaling {
    enabled = false
  }
}

# Primary node pool
resource "google_container_node_pool" "default_node_pool" {
  provider = google-beta  # Enable beta features
  name = "default-node-pool"
  location = local.zone
  cluster = google_container_cluster.primary.name
  initial_node_count  = lookup(var.environment_to_node_count_map, local.environment)
  node_locations = [
    local.zone,
  ]

  autoscaling {
    min_node_count = 2
    max_node_count = 2
  }

  node_config {
    preemptible = false
    machine_type = "e2-small"
    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

# Service account for docker registry access gcr.io
resource "google_service_account" "gcr_registry" {
  account_id = "gcr-${local.environment}"
}

resource "google_service_account_key" "gcr_registry" {
  service_account_id = google_service_account.gcr_registry.name
  public_key_type = "TYPE_X509_PEM_FILE"
}

data "google_client_config" "provider" {}
