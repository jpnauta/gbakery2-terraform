locals {
  disk_size = 20  # GB
}

# Generate password for postgres user account
resource "random_id" "postgresql_user_password" {
  byte_length = 8
}

resource "helm_release" "postgresql" {
  depends_on = [
    # Don't create this until the node pool is created
    google_container_node_pool.default_node_pool
  ]

  name = "postgresql"
  repository = "https://charts.bitnami.com/bitnami"
  chart = "postgresql"
  version = "10.13.2"

  set {
    name = "postgresqlPassword"
    value = random_id.postgresql_user_password.hex
  }

  set {
    // Select Postgres version
    name = "imageTag"
    value = "12-alpine"
  }

  set {
    // Reduce default CPU request
    name = "resources.requests.cpu"
    value = "5m"
  }

  set {
    // Set volume size
    name = "persistence.size"
    value = "${local.disk_size}Gi"
  }
}