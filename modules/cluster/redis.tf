# Generate password for redis user account
resource "random_id" "redis_user_password" {
  byte_length = 8
}

resource "helm_release" "redis" {
  depends_on = [
    # Don't create this until the node pool is created
    google_container_node_pool.default_node_pool
  ]

  name = "redis"
  repository = "https://charts.bitnami.com/bitnami"
  chart = "redis"
  version = "15.5.3"

  set {
    name  = "auth.password"
    value = random_id.redis_user_password.hex
  }

  set {
    // Select Redis version
    name  = "imageTag"
    value = "6-alpine"
  }

  set {
    // Reduce default CPU request
    name  = "master.resources.requests.cpu"
    value = "5m"
  }

  set {
    // Disable clustering
    name  = "architecture"
    value = "standalone"
  }
}