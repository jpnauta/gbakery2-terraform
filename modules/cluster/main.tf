locals {
  app_name = var.app_name
  environment = var.environment
  project = var.project
  region = var.region
  zone = var.zone
  service_account_name = var.service_account_name
  current_access_token = var.current_access_token
}
