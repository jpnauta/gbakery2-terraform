# Create kubernetes resources
# Should mostly be used for creating secrets & config - use Helm for everything
# Useful tool for converting Kubernetes YAML to Terraform: https://github.com/sl1pm4t/k2tf

provider "kubernetes" {
  host = "https://${google_container_cluster.primary.endpoint}"
  token = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth[0].cluster_ca_certificate)
  client_certificate = base64decode(google_container_cluster.primary.master_auth[0].client_certificate)
  client_key = base64decode(google_container_cluster.primary.master_auth[0].client_key)
}

# Add credentials to allow pulling from gcr.io
resource "kubernetes_secret" "docker-registry" {
  depends_on = [
    # Don't create this until the node pool is created
    google_container_node_pool.default_node_pool
  ]

  type = "kubernetes.io/dockerconfigjson"
  metadata {
    name = "gcr-json-key"
    annotations = {
      "kubernetes.io/service-account.name" = google_service_account.gcr_registry.name
    }
  }
  data = {
    ".dockerconfigjson" = base64decode(google_service_account_key.gcr_registry.private_key)
  }
}