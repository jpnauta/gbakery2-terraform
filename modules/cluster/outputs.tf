output "cluster_name" {
  value = google_container_cluster.primary.name
}

output "postgresql_user_password" {
  value = random_id.postgresql_user_password.hex
}

output "redis_user_password" {
  value = random_id.redis_user_password.hex
}

output "web_static_ip" {
  value = google_compute_address.web_static_ip.address
}
