data "google_secret_manager_secret_version" "web-stripe-secret-key" {
  provider = google-beta
  secret = "web-stripe-secret-key"
}

data "google_secret_manager_secret_version" "web-stripe-endpoint-secret" {
  provider = google-beta
  secret = "web-stripe-endpoint-secret"
}

data "google_secret_manager_secret_version" "web-rollbar-access-token" {
  provider = google-beta
  secret = "web-rollbar-access-token"
}

data "google_secret_manager_secret_version" "web-sendgrid-api-key" {
  provider = google-beta
  secret = "web-sendgrid-api-key"
}