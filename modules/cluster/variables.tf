variable "app_name" {
  description = "Name of the application."
  type = string
}

variable "project" {
  description = "The Google Cloud project ID."
  type = string
}

variable "region" {
  description = "The Google Cloud region."
  type = string
}

variable "zone" {
  description = "The Google Cloud zone."
  type = string
}

variable "environment" {
  description = "The project environment."
  type = string
}

variable "service_account_name" {
  type = string
}

variable "current_access_token" {
  description = "Reference to the access token currently used to authenticate the user."
  type = string
}

variable "environment_to_node_count_map" {
  type = map

  default = {
    sandbox = 2
    production = 2
  }
}
