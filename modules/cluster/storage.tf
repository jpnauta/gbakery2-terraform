# GCS bucket for QRCode generator serverless function storage
resource "google_storage_bucket" "images" {
  name = "images-gcs-${local.environment}"
  location = local.region
  force_destroy = true
}
