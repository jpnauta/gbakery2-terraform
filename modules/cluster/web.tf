locals {
  # App config
  web_secret_key = random_id.web_secret_key.hex
  web_postgres_password = random_id.postgresql_user_password.hex
  web_redis_password = random_id.redis_user_password.hex
  web_site_url = "app.glamorganbakery.com"
  dash_site_url = "dash.glamorganbakery.com"
  web_static_url = "https://storage.googleapis.com/gbakery2-app-live-static/"
  web_static_bucket_name = "gbakery2-app-live-static"
  web_media_url = "https://storage.googleapis.com/gbakery2-app-live-media/"
  web_media_bucket_name = "gbakery2-app-live-media"
  web_whitelist = [
    "https://dash.glamorganbakery.com",
    "https://www.glamorganbakery.com",
    "https://app.glamorganbakery.com",
    "https://glamorganbakery.com",
    "http://localhost:1337",
    "http://localhost:8080",
    "https://terrier-ruby-t2nj.squarespace.com",
    "https://www.test.glamorganbakery.com",
    "https://checkout.glamorganbakery.com"
  ]
}

resource "google_service_account" "web" {
  account_id   = "${local.app_name}-web"
  display_name = "${local.app_name} web"
}

# Allows web application to read/write/etc. from GCS
resource "google_project_iam_member" "storage_admin_web" {
  role = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.web.email}"
}

# Generate certificate to be injected into web application
resource "google_service_account_key" "web" {
  service_account_id = google_service_account.web.name
  public_key_type = "TYPE_X509_PEM_FILE"
}

# Generate password for secret key
resource "random_id" "web_secret_key" {
  byte_length = 8
}

# Static IP address to use as ingress/load balancer to the cluster
resource "google_compute_address" "web_static_ip" {
  name = "${local.app_name}-${local.environment}-web-static-ip"
}

resource "kubernetes_config_map" "web-config" {
  metadata {
    name = "web-config"
  }
  data = {
    "STORAGE_TYPE": "gs"
    "GOOGLE_APPLICATION_CREDENTIALS": "/code/credentials.json"
    "ENABLE_COLLECTFAST": "true"
    "SITE_URL": local.web_site_url
    "CORS_ALLOWED_ORIGINS": join(",", local.web_whitelist)
    "GS_PROJECT_ID": local.project
    "MEDIA_URL": local.web_media_url
    "GS_MEDIA_BUCKET_NAME": local.web_media_bucket_name
    "STATIC_URL": local.web_static_url
    "GS_STATIC_BUCKET_NAME": local.web_static_bucket_name
    "DB_BACKUPS_TASK_ENABLED": "true"
    "DASH_SITE_URL": local.dash_site_url
    "SEND_ACTUAL_EMAILS": "true"
    # Place INSENSITIVE web environment variables here ^
  }
}

resource "kubernetes_secret" "web-secrets" {
  metadata {
    name = "web-secrets"
  }
  data = {
    "SECRET_KEY": local.web_secret_key
    "SERVICE_ACCOUNT": base64decode(google_service_account_key.web.private_key)
    "DATABASE_URL": "psql://postgres:${local.web_postgres_password}@postgresql:5432/postgres"
    "CACHE_LOCATION_URL": "redis://redis:${local.web_redis_password}@redis-master:6379/1"
    "CELERY_BROKER_URL": "redis://redis:${local.web_redis_password}@redis-master:6379/0"
    "CHANNELS_REDIS_HOST": "redis://redis:${local.web_redis_password}@redis-master:6379/2"
    "STRIPE_SECRET_KEY": data.google_secret_manager_secret_version.web-stripe-secret-key.secret_data
    "STRIPE_ENDPOINT_SECRET": data.google_secret_manager_secret_version.web-stripe-endpoint-secret.secret_data
    "ROLLBAR_ACCESS_TOKEN": data.google_secret_manager_secret_version.web-rollbar-access-token.secret_data
    "SENDGRID_API_KEY": data.google_secret_manager_secret_version.web-sendgrid-api-key.secret_data
    # Place SENSITIVE web environment variables here ^
  }
}

resource "helm_release" "web" {
  depends_on = [
    # Don't create this until the node pool is created
    google_container_node_pool.default_node_pool
  ]

  name = "web"
  chart = "./charts/web"
  version = "0.1.0"
  wait_for_jobs = true

  set {
    # Unused flag that causes Terraform to force update whenever it is unique
    name = "forceUpdateKey"
    value = timestamp()
  }

  set {
    name = "service.externalIp"
    value = google_compute_address.web_static_ip.address
  }
}
